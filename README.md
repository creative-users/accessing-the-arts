# Accessing the Arts

Accessing the Arts is a pilot initiative that is working towards becoming your go-to source for information about accessible arts and culture. Our goal is to promote accessibility in the arts and showcase how communities are reshaping the arts sector